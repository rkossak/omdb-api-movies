from datetime import date, timedelta
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import Movie, Comment


class TestApi(APITestCase):

    def setUp(self):
        movie1 = Movie.objects.create(title="test", details={})
        movie2 = Movie.objects.create(title="test2", details={})
        for _ in range(5):
            Comment.objects.create(movie=movie1, text="test")
        for _ in range(4):
            Comment.objects.create(movie=movie2, text="test")

    def tearDown(self):
        Movie.objects.all().delete()

    def test_movie_list(self):
        url = reverse('api:movies-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 2)

    def test_comment_list(self):
        comment_count = Comment.objects.count()
        url = reverse('api:comments-list')
        response = self.client.post(
            url,
            {
                "movie": Movie.objects.first().id,
                "text": "test"
            },
            format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), comment_count + 1)

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), comment_count + 1)

    def test_top_blank_date_range(self):
        response = self.client.get(reverse('api:top-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_top_statistics(self):
        date_today = date.today().strftime("%Y-%m-%d")
        response = self.client.get(
            reverse('api:top-list'), {'from': date_today, 'to': date_today}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), Movie.objects.count())
        self.assertEqual(
            response.json()[0]['total_comments'],
            Movie.objects.get(title="test").comments.count()
        )
        self.assertEqual(
            response.json()[0]['rank'],
            1
        )
        self.assertEqual(
            response.json()[1]['total_comments'],
            Movie.objects.get(title="test2").comments.count()
        )
        self.assertEqual(
            response.json()[1]['rank'],
            2
        )

    def test_top_statistics_date_range(self):
        date_today = date.today().strftime("%Y-%m-%d")
        new_comment = Comment.objects.create(
            movie=Movie.objects.get(title="test2"),
            text="test"
        )
        response = self.client.get(
            reverse('api:top-list'), {'from': date_today, 'to': date_today}
        )
        self.assertEqual(
            response.json()[0]['rank'],
            1
        )
        self.assertEqual(
            response.json()[1]['rank'],
            1
        )
        self.assertEqual(
            response.json()[1]['total_comments'],
            Movie.objects.get(title="test2").comments.count()
        )

        new_date = date.today() - timedelta(days=10)
        new_comment.created_at = new_date
        new_comment.save()
        response = self.client.get(
            reverse('api:top-list'), {'from': date_today, 'to': date_today}
        )
        self.assertEqual(
            response.json()[1]['total_comments'],
            4
        )
        self.assertEqual(
            response.json()[1]['rank'],
            2
        )
        response = self.client.get(
            reverse('api:top-list'), {'from': new_date.strftime("%Y-%m-%d"), 'to': new_date.strftime("%Y-%m-%d")}
        )
        self.assertEqual(
            response.json()[0]['total_comments'],
            1
        )
        self.assertEqual(
            response.json()[0]['rank'],
            1
        )
