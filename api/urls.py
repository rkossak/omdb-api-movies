from rest_framework import routers

from api.views import MovieViewSet, CommentViewSet, TopViewSet

router = routers.DefaultRouter()
router.register(r'movies', MovieViewSet, base_name="movies")
router.register(r'comments', CommentViewSet, base_name="comments")
router.register(r'top', TopViewSet, base_name="top")
urlpatterns = router.urls
