from django.db.models import Count
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from api.models import Movie, Comment
from api.serializers import (
    MovieSerializer, CommentSerializer, TopSerializer)
from api.utils import set_rank, get_movie_details


class MovieViewSet(mixins.CreateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('title',)

    @staticmethod
    def perform_create(serializer):
        movie_details_response = get_movie_details(
            serializer.validated_data['title'])
        if 'Error' in movie_details_response:
            raise ValidationError({'error': movie_details_response['Error']})
        serializer.save(
            details=get_movie_details(serializer.validated_data['title'])
        )


class CommentViewSet(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('movie',)


class TopViewSet(mixins.ListModelMixin,
                 viewsets.GenericViewSet):
    queryset = Movie.objects.all()
    serializer_class = TopSerializer

    def list(self, request, *args, **kwargs):
        if not (self.request.query_params.get('from') or
                self.request.query_params.get('to')):
            return Response(
                {'error': "Date range ('from', 'to') is required in url params."},
                status=status.HTTP_400_BAD_REQUEST
            )
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset().filter(
            comments__created_at__range=[
                self.request.query_params['from'],
                self.request.query_params['to']
            ]
        ).values('id').annotate(
            total_comments=Count('comments')
        ).order_by('-total_comments')
        return set_rank(queryset)
