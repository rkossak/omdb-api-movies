from rest_framework import serializers

from api.models import Movie, Comment


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('title', 'details')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('movie', 'text')


class TopSerializer(serializers.ModelSerializer):
    movie_id = serializers.IntegerField(source='id')
    total_comments = serializers.IntegerField()
    rank = serializers.IntegerField()

    class Meta:
        model = Movie
        fields = ('movie_id', 'total_comments', 'rank')
