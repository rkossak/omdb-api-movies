import requests
from django.conf import settings


def get_movie_details(title):
    response = requests.get(
        settings.OMDB_API_URL,
        {
            't': title,
            'apikey': settings.OMDB_API_KEY
        }
    )
    return response.json()


def set_rank(queryset):
    rank = 1
    for index, item in enumerate(queryset):
        if index is not 0:
            previous = queryset[index - 1]
            if not previous.get('total_comments') == item.get('total_comments'):
                rank += 1
        item['rank'] = rank
    return queryset
