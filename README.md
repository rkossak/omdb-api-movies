**Movies api**

## General info
Simple REST API related with movies.

## Technologies
- Django 
- Django Rest Framework

## Requirements:
- docker
- docker-compose
- git 

## Setup
- `git clone https://rkossak@bitbucket.org/rkossak/omdb-api-movies.git`
- `docker-compose build`
- `docker-compose up`
- `docker-compose run api ./manage.py migrate`