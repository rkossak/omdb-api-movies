from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone


class Movie(models.Model):
    title = models.CharField(max_length=100, unique=True)
    details = JSONField()


class Comment(models.Model):
    movie = models.ForeignKey("Movie", related_name='comments', on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateField(default=timezone.now)
